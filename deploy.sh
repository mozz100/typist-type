#!/bin/sh
npm run build

echo "Sync files..."
aws s3 sync --delete build/ s3://content.rmorrison.net/things/typist-type/

echo "CloudFront..."
aws cloudfront create-invalidation --distribution-id E1JP8YOH7GC63R --paths / '/*'

echo "Done."