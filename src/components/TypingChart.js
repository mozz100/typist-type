import React from 'react';
import LetterBar from './LetterBar';
import categorise from '../Categoriser';
import './TypingChart.css';

class TypingChart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            scores: this.props.recorder.getNormalisedScores()
        }
        this.props.recorder.addCallback(this.updateScores)
    }
    updateScores = () => {
        this.setState({
            scores: this.props.recorder.getNormalisedScores()
        })
    }
    render() {
        let nearestMatch = categorise(this.props.recorder);
        let typingChart = <div className="TypingChart">
            <p className="mt-3">
                &#10551; Type the words. &#10548;
            </p>
            <p>Use a physical keyboard!</p>
        </div>;
        if (this.props.recorder.completeness() > 0.99) {
            typingChart = <div className="TypingChart">
                {
                    Object.keys(this.state.scores).map(
                        (k) => {
                            return <LetterBar key={k} label={k} value={this.state.scores[k]} />
                        }
                    )
                }
                <p className="mt-3">Your category: {nearestMatch.name}.</p>
                <p>{ nearestMatch.description }</p>
            </div>
        }
        return typingChart;
    }
}

export default TypingChart;