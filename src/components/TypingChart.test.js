import TypingChart from './TypingChart.js';
import React from 'react';
import ReactDOM from 'react-dom';
import Recorder from '../Recorder.js';

describe('TypingChart', () => {
    let div, result, recorder;

    let typingChart;
    beforeEach(() => {
        recorder = new Recorder();
        div = document.createElement('div');
        typingChart = <TypingChart recorder={recorder} />;
        result = ReactDOM.render(typingChart, div);
    });
    afterEach(() => {
        ReactDOM.unmountComponentAtNode(div);
    });
    it('renders', () => {
        expect(div.querySelectorAll('div.LetterBar').length)
            .toEqual(0);
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("C", Recorder.Shift.RIGHT);
        recorder.record("K", Recorder.Shift.RIGHT);
        recorder.record("T", Recorder.Shift.RIGHT);
        recorder.record("?", Recorder.Shift.RIGHT);
        expect(div.querySelectorAll('div.LetterBar').length)
            .toEqual(Object.keys(recorder.keyCounts).length);
    });
    it('updates', () => {
        result.updateScores();
    })
});
