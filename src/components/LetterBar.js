import React from 'react';
import './LetterBar.css';


class LetterBar extends React.Component {
    render() {
        let marginLeft = this.props.value > 0 ? '50%' : (50 * (1 + this.props.value)) + '%';
        let width = this.props.value > 0 ? 50 * this.props.value + '%' : -50 * this.props.value + '%';
        if (Math.abs(this.props.value) < 0.005) {
            width = '1%';
            marginLeft = '49.5%';
        }
        return (
            <div className="LetterBar text-center mt-3">
                <div className="progress">
                    <div className="progress-bar"
                        style={{
                            width: width,
                            marginLeft: marginLeft
                        }}
                    />
                    <div className="label">
                        <kbd>{this.props.label}</kbd>
                    </div>
                </div>

            </div>
        )
    }
}

export default LetterBar;