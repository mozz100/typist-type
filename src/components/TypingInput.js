import React from 'react';
import './TypingInput.css';
import Recorder from '../Recorder';

// The text is designed to include all the shift-heavy characters found in
// Recorder.js: ACKT?
const sampleText = `
Today's keyboard layout was devised by Christopher Sholes, who lived in Kenosha, USA.
What type of typist are you?
`.trim()

class TypingInput extends React.Component {
    constructor(props) {
        super(props);
        this.textarea = React.createRef();
        this.shiftState = null;
    }
    componentDidMount() {
        this.textarea.focus();
    }
    handleKeyUp = (e) => {
        if (e.which === 16) {  // shift (either)
            // Shift key has been released
            this.shiftState = null;
        }
    }
    handleKeyDown = (e) => {
        if (this.shiftState !== null) {
            // A keypress while shift is pressed.
            this.props.recorder.record(e.key, this.shiftState);
        }

        if (e.location === KeyboardEvent.DOM_KEY_LOCATION_LEFT){
            // Left shift key is going down
            this.shiftState = Recorder.Shift.LEFT;
        } else if (e.location === KeyboardEvent.DOM_KEY_LOCATION_RIGHT){
            // Right shift key is going down
            this.shiftState = Recorder.Shift.RIGHT;
        }
    }

    render() {
        return (<div className="TypingInput row">
            <div className="col">
                {sampleText.split("\n").map(
                    (para, index) => {
                        return <p key={index} className="sampleText">{para}</p>
                    }
                )}
            </div>
            <div className="col">
                <textarea ref={(input) => { this.textarea = input; }} onKeyUp={this.handleKeyUp} onKeyDown={this.handleKeyDown} className="typingText">

                </textarea>
            </div>
        </div>);
    }
}

export default TypingInput;