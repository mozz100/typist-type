import LetterBar from './LetterBar.js';
import React from 'react';
import ReactDOM from 'react-dom';

describe('LetterBar', () => {
    let div, result;

    beforeEach(() => {
        div = document.createElement('div');
        result = ReactDOM.render(<LetterBar value={0.5} label="A" />, div);
    });
    afterEach(() => {
        ReactDOM.unmountComponentAtNode(div);
    });
    it('renders correctly', () => {
        expect(div.querySelectorAll('div.LetterBar').length).toEqual(1);
        expect(div.querySelector('kbd').textContent).toEqual("A");
    })
});
