import TypingInput from './TypingInput.js';
import React from 'react';
import {mount} from 'enzyme';
import Recorder from '../Recorder';

describe('TypingInput', () => {
    let result, recorder;

    beforeEach(() => {
        recorder = new Recorder()
        result = mount(<TypingInput recorder={recorder} />);
    });
    it('has text', () => {
        expect(result.find('p.sampleText').length).toBeGreaterThan(1);
        expect(result.find('p.sampleText').first().text()).toContain("USA");
    })
    it('captures right shift', () => {
        expect(result.instance().shiftState).toBeNull();
        result.find('textarea').simulate('keyDown', {location: KeyboardEvent.DOM_KEY_LOCATION_RIGHT})
        expect(result.instance().shiftState).toBe(Recorder.Shift.RIGHT);
    })
    it('records', () => {
        expect(recorder.keyCounts["A"]).toEqual(0);
        expect(result.instance().shiftState).toBeNull();
        result.find('textarea').simulate('keyDown', {location: KeyboardEvent.DOM_KEY_LOCATION_LEFT})
        expect(result.instance().shiftState).toBe(Recorder.Shift.LEFT);
        result.find('textarea').simulate('keyDown', {key: "A"});
        expect(result.instance().shiftState).toBe(Recorder.Shift.LEFT);
        result.find('textarea').simulate('keyUp', {key: "A"});
        expect(recorder.keyCounts["A"]).toEqual(1);
        result.find('textarea').simulate('keyUp', {which: 16})
        expect(result.instance().shiftState).toBeNull();
    })
});
