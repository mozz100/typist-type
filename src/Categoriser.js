const categories = [
    {
        name: "Lefty lemur",
        description: "You pretty much use the left shift key for everything.",
        scores: {
            "A": -1,
            "C": -1,
            "K": -1,
            "T": -1,
            "?": -1,
        }
    },
    {
        name: "Righty rabbit",
        description: "You pretty much use the right shift key for everything.",
        scores: {
            "A": 1,
            "C": 1,
            "K": 1,
            "T": 1,
            "?": 1,
        }
    },
    {
        name: "Middling molerat",
        description: "There's no strong pattern in your use of the left and right shift keys.",
        scores: {
            "A": 0,
            "C": 0,
            "K": 0,
            "T": 0,
            "?": 0,
        }
    },
    {
        name: "Textbook tiger",
        description: "Whatever symbol you're typing, you tend to use the opposing shift key.",
        scores: {
            "A": 1,
            "C": 1,
            "K": -1,
            "T": 1,
            "?": 1,
        }
    }
]

function distance(recorder, category) {
    // Calculate the 'distance' between a Recorder instance and a category
    let recorderScores = recorder.getNormalisedScores();
    let distance = 0;
    Object.keys(category.scores).forEach((key) => {
        distance += (category.scores[key] - recorderScores[key]) ** 2
    })
    return distance
}

function categorise(recorder) {
    // Return the category closest to the recorder's scores.
    let closestCategory;
    let closestDistance = 99999;
    categories.forEach((category) => {
        let categoryDistance = distance(recorder, category);
        // console.log(category.name, categoryDistance)
        if (categoryDistance < closestDistance) {
            closestDistance = categoryDistance;
            closestCategory = category;
        }
    })
    return closestCategory;
}
export default categorise;