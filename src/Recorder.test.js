import Recorder from './Recorder';

describe('Recorder', () => {
    let recorder;
    beforeEach(() => {
        recorder = new Recorder();
    })
    it('records', () => {
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("A", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.LEFT);
        expect(recorder.keyCounts[Recorder.Keys.A]).toEqual(2);
        expect(recorder.keyScores[Recorder.Keys.A]).toEqual(0);
        expect(recorder.keyCounts[Recorder.Keys["?"]]).toEqual(1);
        expect(recorder.keyScores[Recorder.Keys["?"]]).toEqual(-1);
    })
    it('calculates', () => {
        expect(recorder.getNormalisedScores()[Recorder.Keys.A]).toEqual(0);
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("A", Recorder.Shift.LEFT);
        expect(recorder.getNormalisedScores()[Recorder.Keys.A]).toEqual(0);
        recorder.record("A", Recorder.Shift.LEFT);
        const normalisedScores = recorder.getNormalisedScores();
        expect(normalisedScores[Recorder.Keys.A]).toBeLessThan(-0.332);
        expect(normalisedScores[Recorder.Keys.A]).toBeGreaterThan(-0.334)
    })
    it('rejects', () => {
        expect(
            () => { recorder.record("A", "NOSUCHSHIFT")}
        ).toThrow();
    });
    it('resets', () => {
        recorder.reset();
        expect(recorder.getNormalisedScores()["C"]).toEqual(0)
        expect(recorder.keyCounts[Recorder.Keys.A]).toEqual(0)
    });
    it('calls handers', () => {
        let cb = jest.fn();
        recorder.addCallback(cb);
        expect(cb.mock.calls.length).toEqual(0);
        recorder.record("A", Recorder.Shift.LEFT);
        expect(cb.mock.calls.length).toEqual(1);
    });
    it('ignores', () => {
        let cb = jest.fn();
        recorder.addCallback(cb);
        expect(cb.mock.calls.length).toEqual(0);
        recorder.record("NOSUCHKEY", Recorder.Shift.LEFT)
        expect(cb.mock.calls.length).toEqual(0);
    });
    it('tracks emptiness', () => {
        expect(recorder.completeness()).toEqual(0);
    });
    it('tracks completeness', () => {
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("A", Recorder.Shift.RIGHT);
        expect(recorder.completeness()).toEqual(0.2);
        recorder.record("T", Recorder.Shift.RIGHT);
        expect(recorder.completeness()).toEqual(0.4);
        recorder.record("C", Recorder.Shift.RIGHT);
        recorder.record("K", Recorder.Shift.RIGHT);
        recorder.record("?", Recorder.Shift.RIGHT);
        expect(recorder.completeness()).toEqual(1);
    });

})

