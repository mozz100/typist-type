import React from 'react';
import TypingInput from './components/TypingInput';
import TypingChart from './components/TypingChart';
import Recorder from './Recorder';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.recorder = new Recorder();
  }
  render() {
    return (
      <div className="container">
        <TypingInput recorder={this.recorder} />
        <TypingChart recorder={this.recorder} />
      </div>
    );
  }
}

export default App;
