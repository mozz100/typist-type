
class Recorder {

    constructor() {
        this.reset();
        this.callbacks = [];
    }

    static Shift = Object.freeze({
        LEFT: "left",
        RIGHT: "right",
    });
    static Keys = Object.freeze({
        // Pick keys that might help distinguish the way different typists use
        // their shift modifiers, spread around the left/right/centre of the
        // keyboard.
        "A": "A",
        "C": "C",
        "K": "K",
        "T": "T",
        "?": "?",
    });

    reset() {
        this.keyScores = {};
        this.keyCounts = {};
        Object.keys(Recorder.Keys).forEach((key) => {
            this.keyCounts[Recorder.Keys[key]] = 0;
            this.keyScores[Recorder.Keys[key]] = 0;
        });
    }

    record(key, shft) {
        const lookup = Recorder.Keys[key.toUpperCase()];
        if (lookup === undefined) {
            return
        }
        if (shft === Recorder.Shift.LEFT) {
            this.keyScores[lookup] -= 1;
        } else if (shft === Recorder.Shift.RIGHT) {
            this.keyScores[lookup] += 1;
        } else {
            throw new Error("Unknown shift")
        }
        this.keyCounts[lookup]++;
        this.callbacks.forEach((fn) => fn())
    }

    getNormalisedScores() {
        const normalisedScores = {};
        Object.keys(Recorder.Keys).forEach((key) => {
            const lookup = Recorder.Keys[key];
            if (this.keyCounts[lookup] === 0) {
                // no keypresses - assume zero; this is equivalent to equal L/R
                normalisedScores[lookup] = 0;
            } else {
                normalisedScores[lookup] = this.keyScores[lookup] / this.keyCounts[lookup];
            };
        });
        return normalisedScores;
    }

    completeness() {
        let completeness = 0;
        Object.keys(Recorder.Keys).forEach((key) => {
            const lookup = Recorder.Keys[key];
            if (this.keyCounts[lookup] > 0) {
                completeness++;
            }
        })
        return completeness / Object.keys(Recorder.Keys).length;
    }

    addCallback(fn) {
        this.callbacks.push(fn);
    }
}

export default Recorder;