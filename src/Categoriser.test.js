import categorise from './Categoriser';
import Recorder from './Recorder';

describe('Categoriser', () => {
    it('matches lefty', () => {
        let recorder = new Recorder();
        recorder.record("A", Recorder.Shift.LEFT);
        recorder.record("C", Recorder.Shift.LEFT);
        recorder.record("K", Recorder.Shift.LEFT);
        recorder.record("T", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.LEFT);
        let match = categorise(recorder);
        expect(match.name).toEqual("Lefty lemur");
    })
    it('matches righty', () => {
        let recorder = new Recorder();
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("C", Recorder.Shift.RIGHT);
        recorder.record("K", Recorder.Shift.RIGHT);
        recorder.record("T", Recorder.Shift.RIGHT);
        recorder.record("?", Recorder.Shift.RIGHT);
        let match = categorise(recorder);
        expect(match.name).toEqual("Righty rabbit");
    })
    it('matches middle', () => {
        let recorder = new Recorder();
        let match = categorise(recorder);
        expect(match.name).toEqual("Middling molerat");
    })
    it('matches tiger', () => {
        let recorder = new Recorder();
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("C", Recorder.Shift.RIGHT);
        recorder.record("K", Recorder.Shift.LEFT);
        recorder.record("T", Recorder.Shift.RIGHT);
        recorder.record("?", Recorder.Shift.LEFT);
        let match = categorise(recorder);
        expect(match.name).toEqual("Textbook tiger");
    })
    it('matches tiger near miss', () => {
        let recorder = new Recorder();
        recorder.record("A", Recorder.Shift.RIGHT);
        recorder.record("C", Recorder.Shift.RIGHT);
        recorder.record("K", Recorder.Shift.LEFT);
        recorder.record("T", Recorder.Shift.RIGHT);
        recorder.record("T", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.LEFT);
        recorder.record("?", Recorder.Shift.RIGHT);
        recorder.record("?", Recorder.Shift.RIGHT);
        let match = categorise(recorder);
        expect(match.name).toEqual("Textbook tiger");
    })
})

